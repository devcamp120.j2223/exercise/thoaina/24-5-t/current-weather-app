// REGION 1
var gCity = {
    name: "",
    days: 0,
};
// REGION 2
$("#btn-search-city").on("click", function () {
    onBtnSearchCityClick();
});

$("#btn-search-city-forecast").on("click", function () {
    onBtnSearchCityForeCastClick();
});
// REGION 3
function onBtnSearchCityClick() {
    getData(gCity);
    console.log(gCity.name);
    var vIsValidated = validateData(gCity);
    if (vIsValidated) {
        $.ajax({
            url: 'http://api.openweathermap.org/data/2.5/weather?q=' + gCity.name + "&units=metric" +
                "&APPID=c10bb3bd22f90d636baa008b1529ee25",
            type: "GET",
            dataType: "json",
            success: function (data) {
                showData(data);
                console.log(data)
            }

        });
    }
}

function onBtnSearchCityForeCastClick() {
    getDataForeCast(gCity);
    console.log(gCity);
    var vIsValidated = validateData(gCity);
    if (vIsValidated) {
        $.ajax({
            url: 'http://api.openweathermap.org/data/2.5/forecast/daily?q=' + gCity.name + "&units=metric" +
                "&cnt=" + gCity.days + "&APPID=c10bb3bd22f90d636baa008b1529ee25",
            type: "GET",
            dataType: "json",
            success: function (data) {
                showDataForecast(data);
                console.log(data.list)
            }
        });
    }
}
// REGION 4
function getData(paramObj) {
    paramObj.name = $("#inp-city-name").val();
}

function getDataForeCast(paramObj) {
    paramObj.name = $("#inp-city-name-forecast").val();
    paramObj.days = $("#inp-days").val();
}

function validateDataForeCast(paramObj) {
    var vIsValidated = true;
    if (!paramObj.name) {
        vIsValidated = false;
        alert("Bạn chưa nhập city name");
    }
    if (!paramObj.days) {
        vIsValidated = false;
        alert("Bạn chưa nhập days");
    }
    return vIsValidated;
}

function validateData(paramObj) {
    var vIsValidated = true;
    if (!paramObj.name) {
        vIsValidated = false;
        alert("Bạn chưa nhập city name");
    }
    return vIsValidated;
}

function showData(data) {
    var vDivShowData = $("#infor-weather");
    $("<h2>", {
        text: `Current Weather for ${data.name}, ${data.sys.country}`
    }).addClass("text-center").appendTo(vDivShowData);
    $("<p>", {
        text: `Weather: ${data.weather[0].main}`
    }).appendTo(vDivShowData);
    $("<p>", {
        text: `Description: ${data.weather[0].description}`
    }).appendTo(vDivShowData);
    $("<p>", {
        text: `Temperature: ${data.main.temp} `
    }).appendTo(vDivShowData);
    $("<p>", {
        text: `Pressure: ${data.main.pressure} hpa`
    }).appendTo(vDivShowData);
    $("<p>", {
        text: `Humidity: ${data.main.humidity}%`
    }).appendTo(vDivShowData);
    $("<p>", {
        text: `Min Temperature: ${data.main.temp_min}`
    }).appendTo(vDivShowData);
    $("<p>", {
        text: `Max Temperature: ${data.main.temp_max}`
    }).appendTo(vDivShowData);
    $("<p>", {
        text: `Wind Speed: ${data.wind.speed}m/s`
    }).appendTo(vDivShowData);
    $("<p>", {
        text: `Wind Direction: ${data.wind.deg}`
    }).appendTo(vDivShowData);
}

function showDataForecast(data) {
    var vDay = 1;
    var vTable = $("#table-weather");
    for (var bIndex = 0; bIndex < data.list.length; bIndex++) {
        var vNewRow = $("<tr>").appendTo(vTable);
        $("<td>", {
            text: vDay++,
        }).appendTo(vNewRow);
        var vColIcon = $("<td>").appendTo(vNewRow);
        $("<img>", {
            src: "http://openweathermap.org/img/w/" + data.list[bIndex].weather[0].icon + ".png"
        }).appendTo(vColIcon);
        $("<td>", {
            text: data.list[bIndex].weather[0].main,
        }).appendTo(vNewRow);
        $("<td>", {
            text: data.list[bIndex].weather[0].description,
        }).appendTo(vNewRow);
        $("<td>", {
            text: data.list[bIndex].temp.morn,
        }).appendTo(vNewRow);
        $("<td>", {
            text: data.list[bIndex].temp.night,
        }).appendTo(vNewRow);
        $("<td>", {
            text: data.list[bIndex].temp.min,
        }).appendTo(vNewRow);
        $("<td>", {
            text: data.list[bIndex].temp.max,
        }).appendTo(vNewRow);
        $("<td>", {
            text: data.list[bIndex].pressure,
        }).appendTo(vNewRow);
        $("<td>", {
            text: data.list[bIndex].humidity,
        }).appendTo(vNewRow);
        $("<td>", {
            text: data.list[bIndex].speed,
        }).appendTo(vNewRow);
        $("<td>", {
            text: data.list[bIndex].deg,
        }).appendTo(vNewRow);

    }
}